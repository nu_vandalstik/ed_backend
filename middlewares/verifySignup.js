const db = require("../models");

const ROLES = db.ROLES;
const User = db.user;

checkDuplicateUsernameOrEmail = (req, res, next) => {
  User.findOne({
    email: req.body.email,
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({
        message: err,
      });
      return;
    }
    if (user) {
      res.status(400).send({
        message: "Username Sudah Ada Gunakan Username Lain!",
      });
      return;
    }
    User.findOne({
      phone: req.body.phone,
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({
          message: err,
        });
        return;
      }
      if (user) {
        res.status(400).send({
          message: "Nomer Telpon Sudah Ada Gunakan Nomer Lain!",
        });
        return;
      }
      next();
    });
  });
};


checkRoleExisted = (req, res, next) => {
    if (req.body.roles) {
      for (let i = 0; i < req.body.roles.length; i++) {
        //db.ROLES = ["user", "admin", "vendor"] === "penjual"
  
        if (!ROLES.includes(req.body.roles[i])) {
          res.status(400).send({
            message: `Role ${req.body.roles[i]} tidak ada`,
          });
          return;
        }
      }
    }
    next()
  };
  
  
  const verifySignUp ={
      checkDuplicateUsernameOrEmail,
      checkRoleExisted
  }
  
  module.exports = verifySignUp