const config = require("../config/auth.config");
const db = require("../models");

const User = db.user;
const Role = db.role;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    phone: req.body.phone,
  });
  user.save((err, user) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    } else if (req.body.roles) {
      Role.find({ name: { $in: req.body.roles } }, (err, roles) => {
        if (err) {
          return res.status(500).send({
            message: err,
          });
        }

        user.roles = roles.map((role) => role._id);
        user.save((err) => {
          if (err) {
            return res.status(500).send({
              message: err,
            });
          }
          res.send({
            message: " User Telah Berhasil ditambahkan",
          });
        });
      });
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          return res.status(500).send({
            message: err,
          });
        }
        user.roles = [role._id];
        user.save((err) => {
          if (err) {
            return res.status(500).send({
              message: err,
            });
          }
          res.send({
            message: "user telah berhasil terdaftar",
          });
        });
      });
    }
  });
};
