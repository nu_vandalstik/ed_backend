const express = require("express");
const app = express();
const port = 3000;
const cors = require("cors");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const db = require("./models/index"); // kalau nama index, gausah kasih tau nama file
const Role = db.role;

const userRoutes = require('./routes/auth')



require("dotenv").config();
var corsOptions = {
  origin: "*",
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

function init() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err & (count == 0)) {
      new Role({
        name: "user",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }
        console.log("berhasil menambah role user");
      });

      new Role({
        name: "admin",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }
        console.log("berhasil menambah role admin");
      });

      new Role({
        name: "vendor",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }
        console.log("berhasil menambah role vendor");
      });
    }
  });
}





mongoose
  .connect(`${process.env.mongoose_link}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(port, console.log("koneksi berhasil di port " + port)), init();
  })
  .catch((err) => console.log(err));

app.use(cors(corsOptions));
// app.use("v1/auth",  require('./routes/auth_routes') )

require('./routes/auth_routes')(app)
app.get("/", (req, res) => {
  res.send("Hello World!");
});
