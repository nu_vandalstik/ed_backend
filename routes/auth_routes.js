const express = require("express");
const router = express.Router();
const app = express
var bodyParser = require('body-parser');// get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing 
const verifySignUp = require("../middlewares/verifySignup")
const authController = require("../controller/auth")


module.exports = function(app){
    app.use(function (req,res,next) {
        res.header(
            "Access-Control-Allow-headers",
            "Authorization, Origin, Content-type, Accept"
    
        )
        next()
    })

    app.post(
        "/register",
        [
            verifySignUp.checkDuplicateUsernameOrEmail,
            verifySignUp.checkRoleExisted
        ],
        authController.signup
    )


}