const express = require("express");
const router = express.Router();

const verifySignUp = require("../middlewares/verifySignup")
const authController = require("../controller/auth")



router.post("/register", [
    verifySignUp.checkDuplicateUsernameOrEmail,
    verifySignUp.checkRoleExisted
],
authController.signup)

module.exports = router

