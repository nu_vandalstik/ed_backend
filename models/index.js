const db = {};

db.code = require("./code_reference_model");
db.forms = require("./forms_model");
db.responds = require("./responds_model");
db.role = require("./roles_model");
db.talentGroup = require("./talent_groups_model");
db.talent = require("./talents_model");
db.user = require("./user_model");

db.ROLES = ["user", "admin", "vendor"];

module.exports = db;
