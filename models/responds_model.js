const mongoose = require('mongoose')

const responds = mongoose.model(
    "responds",
    new mongoose.Schema({
        userId: mongoose.Schema.Types.ObjectId,
        name:String,
        email:String,
        phone:String,
        code:String,
        isDone: Boolean,
        results:[
            {
                type: mongoose.Schema.Types.ObjectId,
                ref : "talents",
                score:Number,
                totalQuestion:Number
            }
        ],
        responds:[{
          talentId: {
                type: mongoose.Schema.Types.ObjectId,
                ref : "talents",
            },  
            questionId: { 
                type: mongoose.Schema.Types.ObjectId,
                ref : "forms" },
                value: Number,
        }]
    })
)

module.exports = responds