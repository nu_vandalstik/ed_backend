const mongoose = require("mongoose");

const Forms = mongoose.model(
  "Forms",
  new mongoose.Schema({
    title: String,
    question: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "talents",
        reverseFlag: {
          type: Boolean,
          default: false,
        },
        timestamps: true,
        createdBy: mongoose.Schema.Types.ObjectId,
        ref:"users",
        updateBy: mongoose.Schema.Types.ObjectId,
        ref:"users"
      },
    ],
    createdBy: mongoose.Schema.Types.ObjectId,
    updatedBy: mongoose.Schema.Types.ObjectId,
  },
  {
    timestamps: true,
  })
);

module.exports = Forms;
