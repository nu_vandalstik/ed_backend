const mongoose = require('mongoose')

const Talents = mongoose.model(
    "Talents",
    new mongoose.Schema({
        name:String,
        description:String,
        priorOrder:Number,
        groupId:[{
            type:mongoose.Schema.Types.ObjectId,
            ref:"talentGroups"
        }],
    },
    {
        timestamps:true,
    },{
        createdBy:[
            {
                type: mongoose.Schema.Types.ObjectId,
                ref : "users"
            }
        ],
        updatedBy:[
            {
                type: mongoose.Schema.Types.ObjectId,
                ref : "users"
            }
        ]

    })
)

module.exports = Talents