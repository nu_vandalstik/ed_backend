const mongoose = require("mongoose");

const talentGroups = mongoose.model(
  "Talents Group",
  new mongoose.Schema(
    {
      name: String,
      description: String,
      brainSide: String,
      talents: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "talents",
        },
      ],
    },
    {
      timestamps: true,
    },
    {
      createdBy: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "users",
        },
      ],
      updateBy: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "users",
        },
      ],
    }
  )
);

module.exports;
