const mongoose = require('mongoose')

// import mongoose from 'mongoose';
  const { Schema } = mongoose;

  const codeReferenceSchema = new Schema({
    code:String,
    userId: [{
        type: Schema.Types.ObjectId,
        ref: "users"
    }],
    maxResponds: Number,
    createdBy:[{
        type: Schema.Types.ObjectId,
        ref : "users"
    }],
    updateBy: [{
        type: Schema.Types.ObjectId,
        ref : "users"
    }]
  },
  {
    timestamps:true,
  });


// const codeReference = mongoose.model(
//     "codeReference",
//     new mongoose.Schema({
//         code:String,
//         userId: [{
//             type: mongoose.Schema.Types.ObjectId,
//             ref: "users"
//         }],
//         maxResponds: Number,
//         timestamps:true,
//         createdBy:[{
//             type: mongoose.Schema.Types.ObjectId,
//             ref : "users"
//         }],
//         updateBy: [{
//             type: mongoose.Schema.Types.ObjectId,
//             ref : "users"
//         }]
//     }),
    
// )


const codeReference = mongoose.model('CodeReference', codeReferenceSchema);
module.exports = codeReference
